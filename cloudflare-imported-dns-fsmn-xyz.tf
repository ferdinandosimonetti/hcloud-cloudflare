resource "cloudflare_record" "terraform_managed_resource_c93d22b9bbca458dd9574d483ed6b6e0" {
  name    = "em5028"
  proxied = false
  ttl     = 1
  type    = "CNAME"
  value   = "u29985792.wl043.sendgrid.net"
  zone_id = "f33618f805eeda8001ac9c49f5dac6e0"
}

resource "cloudflare_record" "terraform_managed_resource_08db9c5a28c1a08eadc478078d0e894a" {
  name    = "s1._domainkey"
  proxied = false
  ttl     = 1
  type    = "CNAME"
  value   = "s1.domainkey.u29985792.wl043.sendgrid.net"
  zone_id = "f33618f805eeda8001ac9c49f5dac6e0"
}

resource "cloudflare_record" "terraform_managed_resource_54b27f9f80ea4cea76ab3d5a53e2576d" {
  name    = "s2._domainkey"
  proxied = false
  ttl     = 1
  type    = "CNAME"
  value   = "s2.domainkey.u29985792.wl043.sendgrid.net"
  zone_id = "f33618f805eeda8001ac9c49f5dac6e0"
}

resource "cloudflare_record" "terraform_managed_resource_6b956a8a4a9082ada6127dfed10812ef" {
  name     = "fsmn.xyz"
  priority = 98
  proxied  = false
  ttl      = 1
  type     = "MX"
  value    = "route3.mx.cloudflare.net"
  zone_id  = "f33618f805eeda8001ac9c49f5dac6e0"
}

resource "cloudflare_record" "terraform_managed_resource_34f35e195ba7d2e82fcbeb0d6e43a2bb" {
  name     = "fsmn.xyz"
  priority = 33
  proxied  = false
  ttl      = 1
  type     = "MX"
  value    = "route2.mx.cloudflare.net"
  zone_id  = "f33618f805eeda8001ac9c49f5dac6e0"
}

resource "cloudflare_record" "terraform_managed_resource_672802af3511d5e91933be955ee11001" {
  name     = "fsmn.xyz"
  priority = 4
  proxied  = false
  ttl      = 1
  type     = "MX"
  value    = "route1.mx.cloudflare.net"
  zone_id  = "f33618f805eeda8001ac9c49f5dac6e0"
}

resource "cloudflare_record" "terraform_managed_resource_778e3385d879fcc0116f1703e6298ad0" {
  name    = "_dmarc"
  proxied = false
  ttl     = 1
  type    = "TXT"
  value   = "v=DMARC1; p=reject; sp=reject; adkim=s; aspf=s;"
  zone_id = "f33618f805eeda8001ac9c49f5dac6e0"
}

resource "cloudflare_record" "terraform_managed_resource_b21f2e1f52d1ab384d70d960f2d7ffa6" {
  name    = "*._domainkey"
  proxied = false
  ttl     = 1
  type    = "TXT"
  value   = "v=DKIM1; p="
  zone_id = "f33618f805eeda8001ac9c49f5dac6e0"
}

resource "cloudflare_record" "terraform_managed_resource_e87e401e0d8a3143e4eabd65d8f987da" {
  name    = "fsmn.xyz"
  proxied = false
  ttl     = 1
  type    = "TXT"
  value   = "v=spf1 include:_spf.mx.cloudflare.net ~all"
  zone_id = "f33618f805eeda8001ac9c49f5dac6e0"
}

