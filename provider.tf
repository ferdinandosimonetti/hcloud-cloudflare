terraform {
    required_version = "~> 1.3.4"
  	backend "remote" {
		organization = "fsimonetti-eu" # org name from step 2.
		workspaces {
			name = "hcloud-cloudflare" # name for your app's state.
		  }
	  }
    required_providers {
    hcloud = {
      source = "hetznercloud/hcloud"
      version = "1.33.1"
    }
    cloudflare = {
      source = "cloudflare/cloudflare"
      version = "3.9.1"
    }
  }
}
provider "hcloud" {
  # Configuration options
}
provider "cloudflare" {
  # Configuration options
}