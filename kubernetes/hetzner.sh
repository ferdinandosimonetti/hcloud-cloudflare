kubectl apply -f https://raw.githubusercontent.com/rancher/local-path-provisioner/v0.0.24/deploy/local-path-storage.yaml

kubectl -n kube-system create secret generic hcloud --from-literal=token=fS4TLRIiun3iqRDBOpCWbock7tDQkga58x4SJ749pmaZpbnoB1ADwx9WgqDmtzZX
helm repo add hcloud https://charts.hetzner.cloud
helm repo update hcloud
helm install hccm hcloud/hcloud-cloud-controller-manager -n kube-system

kubectl -n kube-system create secret generic hcloud-csi --from-literal=token=fS4TLRIiun3iqRDBOpCWbock7tDQkga58x4SJ749pmaZpbnoB1ADwx9WgqDmtzZX
kubectl apply -f https://raw.githubusercontent.com/hetznercloud/csi-driver/v2.3.2/deploy/kubernetes/hcloud-csi.yml