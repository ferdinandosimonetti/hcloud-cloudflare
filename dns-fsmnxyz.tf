data "cloudflare_zone" "fsmnxyz" {
  name = "fsmn.xyz"
}

resource "tls_private_key" "fsmnxyz" {
  algorithm = "RSA"
}
resource "cloudflare_zone_settings_override" "fsmnxyz-settings" {
  zone_id = data.cloudflare_zone.fsmnxyz.id
  settings {
    tls_1_3 = "on"
    automatic_https_rewrites = "on"
    always_use_https = "on"
    ssl = "flexible"
  }
}

