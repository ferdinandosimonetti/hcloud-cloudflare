resource "hcloud_network" "mynet" {
  name     = "mynet"
  ip_range = "10.111.0.0/16"
}
resource "hcloud_network_subnet" "mailnet" {
  network_id   = hcloud_network.mynet.id
  type         = "cloud"
  network_zone = "eu-central"
  ip_range     = "10.111.1.0/24"
}

resource "hcloud_server" "mail" {
  name        = "test00"
  server_type = "cx31"
  image       = "debian-11"
  location    = "nbg1"
  ssh_keys    = [data.hcloud_ssh_key.test.id]
  user_data   = file("./cloud-init/cloud-init-generic.yml")
  labels = {
    project = "ispconfig"
  }
}
resource "hcloud_server" "test01" {
  name        = "test01"
  server_type = "cx11"
  image       = "debian-11"
  location    = "nbg1"
  ssh_keys    = [data.hcloud_ssh_key.test.id]
  user_data   = file("./cloud-init/cloud-init-generic.yml")
  labels = {
    project = "rancher"
  }
}
resource "hcloud_server" "test02" {
  name        = "test02"
  server_type = "cx11"
  image       = "debian-11"
  location    = "nbg1"
  ssh_keys    = [data.hcloud_ssh_key.test.id]
  user_data   = file("./cloud-init/cloud-init-generic.yml")
  labels = {
    project = "rancher"
  }
}

resource "cloudflare_record" "test00" {
  name    = "test00.mailtest"
  value   = hcloud_server.test00.ipv4_address
  zone_id = data.cloudflare_zone.fsmnxyz.id
  type    = "A"
  proxied = false
}
resource "cloudflare_record" "test01" {
  name    = "test01.mailtest"
  value   = hcloud_server.test01.ipv4_address
  zone_id = data.cloudflare_zone.fsmnxyz.id
  type    = "A"
  proxied = false
}
resource "cloudflare_record" "test02" {
  name    = "test02.mailtest"
  value   = hcloud_server.test02.ipv4_address
  zone_id = data.cloudflare_zone.fsmnxyz.id
  type    = "A"
  proxied = false
}


resource "hcloud_server_network" "test00net" {
  server_id  = hcloud_server.test00.id
  network_id = hcloud_network.mynet.id
  ip         = "10.111.1.20"
}
resource "hcloud_server_network" "test01net" {
  server_id  = hcloud_server.test01.id
  network_id = hcloud_network.mynet.id
  ip         = "10.111.1.21"
}
resource "hcloud_server_network" "test02net" {
  server_id  = hcloud_server.test02.id
  network_id = hcloud_network.mynet.id
  ip         = "10.111.1.22"
}