resource "cloudflare_record" "gitlab-lb0" {
  name    = "fsmngitlb0"
  value   = hcloud_server.gitlab-lb0.ipv4_address
  zone_id = data.cloudflare_zone.fsmnxyz.id
  type    = "A"
  proxied = false
}